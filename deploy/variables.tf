variable prefix {
  type        = string
  default     = "raad"
  description = "prefix all our resources"
}
variable project {
  type        = string
  default     = "recipe-app-api-devops"
  description = "the name of the project"
}

variable contact {
  type        = string
  default     = "umagloire99@gmail.com"
  description = "Address email of the company"
}


